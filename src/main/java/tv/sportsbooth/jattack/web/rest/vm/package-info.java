/**
 * View Models used by Spring MVC REST controllers.
 */
package tv.sportsbooth.jattack.web.rest.vm;
